steps and command for windows

install node.js 
[node.js](https://nodejs.org/en/)

install git (exe)
[git](https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git)

open terminal and check version

`git --version`

open i browser link
[bitbucket](https://bitbucket.org/l_buzya/hw/src/master/)

copy command to clone repo
`git clone https://bitbucket.org/l_buzya/hw.git`

go to directory on your PC where you will storage your work
open Gig Bash, press 'shift+insert' to paste command and press 'enter'

then in terminal type
`cd HW` + 'enter' or go to he directory and open one more time terminal

Then we will create new branch with your name (lfilippova replace by your branch name, first letter name and surname)
`git checkout -b lfilippova`

create directory according number of homework, make your home work, after all need to commit result
`git add -A` -- will add all yours files
`git commit -m 'homework #`

`git log` - will show the last commit 
`git status` - will show if your have some uncommitted files (to have the possibility to push data the message 
 should be 'nothing to commit, working directory clean')

Then push data to bitbucket from your branch name, I'll type my as example
`git push origin lfilippova`

After go to bitbucket and check branch list, will appear your branch
[branch list](https://bitbucket.org/l_buzya/hw/branches/)

In table row with your branch will be able button create click and create pull request by me (l_buzya)
